# Netrunner Proxy

This is a simple web app to get a pdf of cards from a deck on [NetrunnerDB](https://netrunnerdb.com).

It's live at [https://netrunner-proxy.herokuapp.com/](https://netrunner-proxy.herokuapp.com/)

## Built With

[Prawn](https://github.com/prawnpdf/prawn) - for generating PDF files

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
