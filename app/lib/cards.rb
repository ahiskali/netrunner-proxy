module Cards
  API_LINK = 'https://netrunnerdb.com/api/2.0/public/card/'
  IMG_LINK_PREFIX = 'https://netrunnerdb.com/card_image/'
  IMG_LINK_SUFFIX = '.png'

  def self.from(decklist)
    decklist.map do |card_id, quantity|
      [open(image_link(card_id)), quantity]
    end
  end

  private

  def self.image_link(card_id)
    HTTParty.get(API_LINK + card_id)["data"][0]["image_url"] || 
    [IMG_LINK_PREFIX, card_id, IMG_LINK_SUFFIX].join
  end

end