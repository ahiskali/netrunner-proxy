class ProxyController < ActionController::Base
  def show
  end

  def new
    /(?<id>\d+)/ =~ params[:id]
    if (params[:quantity])
      decklist = {id => params[:quantity].to_i}
    else
      decklist = fetch_decklist(id, params[:private])
    end
    
    send_data PdfGenerator.generate(Cards.from(decklist)), filename: "cards.pdf"
  end

  private

  def fetch_decklist(deck_id, is_private)
    link = "https://netrunnerdb.com/api/2.0/public/deck" + (is_private ? "/" : "list/")
    HTTParty.get(link + deck_id)["data"][0]["cards"]
  end
end
